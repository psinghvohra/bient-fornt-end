import React, { Component } from 'react';
import '../CSS/Feeds.css';
import { getFromStorage, setInStorage } from "../../Controller/utils/storage";

class Dashboard extends Component {
    state = {
        signUpFirstName: "",
        signUpLastName: "",
        signUpUserName: "",
        signUpEmail: ""
    };
    render() {
        return (
            <div className="parent1">
                <Feeds />
                <Utility />
            </div>
        );
    }
}

export default Dashboard;

class Feeds extends Component {
    state = {}

    render() {
        return (
            <div className='newsfeeds'>
                {<ul>
                    <li class="dropdown">
                        <a href="" class="dropbtn">Sort</a>
                        <div class="dropdown-content">
                            <a href="">High Rating</a>
                            <a href="">Low Rating</a>
                            <a href="">Relevance</a>
                            <a href="">Time</a>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a href="" class="dropbtn">Filters</a>
                        <div class="dropdown-content">
                            <a href="">India</a>
                            <a href="">USA</a>
                            <a href="">China</a>
                            <a href="">Russia</a>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a href="" class="dropbtn">Genre</a>
                        <div class="dropdown-content">
                            <a href="">Entertainment</a>
                            <a href="">Sports</a>
                            <a href="">Politics</a>
                            <a href="">International</a>
                            <a href="">Science</a>
                            <a href="">Technology</a>
                        </div>
                    </li>
                </ul>

                }
                <div id='newsBlock'>

                    <div class="news">
                        <h1>news1</h1>
                        <button className="dislike">
                            <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                        </button>

                        <button className="like">
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                        </button>

                    </div>
                    <div class="news">
                        <h1>news2</h1>
                        <button className="dislike">
                            <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                        </button>

                        <button className="like">
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                        </button>

                    </div>
                    <div class="news">
                        <h1>news3</h1>
                        <button className="dislike">
                            <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                        </button>

                        <button className="like">
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                        </button>

                    </div>
                    <div class="news">
                        <h1>news4</h1>
                        <button className="dislike">
                            <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                        </button>

                        <button className="like">
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                        </button>

                    </div>
                    <div class="news">
                        <h1>news5</h1>
                        <button className="dislike">
                            <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                        </button>

                        <button className="like">
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                        </button>

                    </div>
                    <div class="news">
                        <h1>news6</h1>
                        <button className="dislike">
                            <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                        </button>

                        <button className="like">
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                        </button>

                    </div><div class="news">
                        <h1>news7</h1>
                        <button className="dislike">
                            <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                        </button>

                        <button className="like">
                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                        </button>

                    </div><div class="news">
                        <h1>news8</h1>
                    </div>
                    <div class="news">
                        <h1>news5</h1>
                    </div><div class="news">
                        <h1>news5</h1>
                    </div>

                </div>
            </div>
        );
    }
}

class Utility extends Component {
    state = {}
    logout = e => {
        e.preventDefault();
        const obj = getFromStorage("the_main_app");
        if (obj && obj.token) {
            const { token } = obj;
            // Verify token
            fetch("http://0.0.0.0:8080/api/account/logout?token=" + token)
                .then(res => res.json())
                .then(json => {
                    if (json.success) {
                        this.setState({
                            token: "",
                            isLoading: false
                        });
                        console.log("logged out")
                    } else {
                        this.setState({
                            isLoading: false
                        });
                    }
                    window.location.href = "/";

                });
        } else {
            this.setState({
                isLoading: false
            });
        }
    }
    render() {
        return (
            <div className='util'>
                <div class="card">
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>
                    <h1>Pawan Singh Vohra</h1>
                    <p class="title">Software Developer</p>
                    <p>Amity University</p>
                    <a href="#"><i class="fa fa-dribbble"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-linkedin"></i></a>
                    <a href="#"><i class="fa fa-facebook"></i></a>

                    <form onSubmit={this.logout}>
                        <input type='submit' value="logout"></input>
                    </form>
                </div>
                <div id='newsinput'>
                    <form>
                        <div>
                            <textarea cols="10" rows="5" charswidth="23" name="text_body" class="field-style" placeholder="Input News"></textarea>

                        </div>
                        <div>
                            <button >Upload</button>

                        </div>
                    </form>
                </div>
            </div>
        );
    }
}
