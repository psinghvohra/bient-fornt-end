import React, { Component } from "react";
import intropic from "../Resources/intro.jpg";
import "../HomePage/HomePage.css";
import { Link } from "react-router-dom/cjs/react-router-dom";
import { getFromStorage, setInStorage } from "../Controller/utils/storage";

class HomePage extends Component {
  state = {};
  render() {
    return (
      <div className="body">
        <Introduction />
        <SignIn />
      </div>
    );
  }
}

class Introduction extends Component {
  state = {};
  render() {
    return (
      <div className="introduction">
        <img id="intro" src={intropic} alt="" />
      </div>
    );
  }
}

class SignIn extends Component {
  state = {
    userName: "",
    password: "",
    signInError: ""
  };

  submit = e => {
    e.preventDefault();
    const { userName, password } = this.state;

    this.setState({
      isLoading: true
    });

    // POST request to backend

    fetch("http://0.0.0.0:8080/api/account/signin", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        userName: userName,
        password: password
      })
    })
      .then(res => res.json())
      .then(json => {
        console.log("json", json);
        if (json.success) {
          setInStorage("the_main_app", { token: json.token });
          this.setState({
            signInError: json.message,
            isLoading: false,
            // signInEmail: "",
            // signInPassword: "",
            token: json.token
          });
          window.location.replace("/dashboard");
        } else {
          this.setState({
            signInError: json.message,
            isLoading: false
          });
        }
        if (!json.success) {
          alert(this.state.signInError);
        }
      });
  };
  changehandler = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    const { userName, password, signInError } = this.state;
    return (
      <div className="signin">
        <div className="sign">
          <form onSubmit={this.submit}>
            <div>
              <label>Username:</label>
              <input
                type="text"
                name="userName"
                value={userName}
                onChange={this.changehandler}
              />
            </div>
            <div>
              <label>Password:</label>
              <input
                type="password"
                name="password"
                value={password}
                onChange={this.changehandler}
              />
            </div>
            <div>
              <input type="submit" value="SignIn" />
            </div>
            <div className="link">
              <Link to="/forgotPassword"> Forgot Username or Password?</Link>
            </div>
            <div className="link">
              <Link to="/SignUp">Create new Account</Link>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default HomePage;
