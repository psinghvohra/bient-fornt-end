import React, { Component } from "react";
import logo from "../src/Resources/logo.png";
import '../src/NavigationBar.css';
class NavigationBar extends Component {
  render() {
    return (
      <div className="navigation">
        <div>
          <a  href="/">
            <img id="logo" src={logo} alt="Logo" />
          </a>
        </div>
      </div>
    );
  }
}
export default NavigationBar;
