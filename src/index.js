import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Parent from "../src/Parent.js";
import SignUp from "./SignUp/SignUp";
import Forget, { ResetPassword } from "./Forget/Forget";
// import Acknowledgement from "../src/Forget/Acknowledgement";
import Confirmation from "./SignUp/Confirmation";
import Dashboard from "./NewsBox/Components/Dashboard";
import HomePage from "./HomePage/HomePage";
import {VerifyConfirmation} from './SignUp/Confirmation'

ReactDOM.render(
  <Router>
    <Parent>
      <Switch>
        <Route path="/" exact component={HomePage} />
        <Route path="/SignUp" component={SignUp} />
        <Route path="/resetPassword" component={ResetPassword} />
        <Route path="/forgotPassword" component={Forget} />
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/confirmation" component={VerifyConfirmation} />
      </Switch>
    </Parent>
  </Router>,
  document.getElementById("root")
);
