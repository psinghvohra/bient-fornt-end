import React from "react";
import NavigationBar from "../src/NavigationBar.js";
import "../src/index.css";
const Parent = ({ children }) => (
  <>
    <NavigationBar />
    <main id="main">{children}</main>
  </>
);
export default Parent;