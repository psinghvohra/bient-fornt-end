import React, { Component } from "react";
import axios from "axios";
import "../Forget/Forget.css";
import { Link } from "react-router-dom/cjs/react-router-dom";

class Forget extends Component {
  state = {
    email: "",
    message:""
  };

  submit = e => {
    e.preventDefault();
    const { email } = this.state;

    fetch("http://0.0.0.0:8080/api/account/forgot", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "host":"http://localhost:3000"
      },
      body: JSON.stringify({
        email: email
      })
    })
      .then(res => res.json())
      .then(json => {
        //console.log("json", json);
        if (json.success) {
          this.setState({
            message: json.message
          });
        } else {
          this.setState({
            message: json.message
          });
        }
        alert(this.state.message);
      });
  };
  changehandler = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  render() {
    const { email, message } = this.state;

    return (
      <div className="forget">
        <div className="box">
          <form onSubmit={this.submit}>
            <div>
              <label>Email:</label>
              <input
                type="text"
                name="email"
                value={email}
                onChange={this.changehandler}
              />
            </div>
            <div>
              <input type="submit" value="Find Me" />
            </div>
          </form>
        </div>
      </div>
    );
  }
}
export default Forget;
class ResetPassword extends Component {

state = {
    password: "",
    confirmPassword: "",
    message: ""
  };
  submit = e => {
    e.preventDefault();
    const { password, confirmPassword } = this.state;
    //const { confirmPassword } = this.state;

    fetch("http://0.0.0.0:8080/api/account/reset", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        password: password
      })
    })
      .then(res => res.json())
      .then(json => {
        //console.log("json", json);
        if (json.success) {
          this.setState({
            message: json.message
          });
        } else {
          this.setState({
            message: json.message
          });
        }
      });
  }
  changehandler = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() { 
    const { password, confirmPassword, message } = this.state;

    return (
      <div className="forget">
        <div className="box">
        {message ? <p>{message}</p> : null}
        <form onSubmit={this.submit}>
        <label>Password</label>    
        <input
                type="password"
                name="password"
                value={password}
                onChange={this.changehandler}
              />
        <br />
        <label>confirm Password</label>        
        <input
                type="password"
                name="confirmPassword"
                value={confirmPassword}
                onChange={this.changehandler}
              />
        <br />
        <input type="submit" value="submit"></input>
        <br />
        <br />
        <Link to="/">Login</Link>
        <br />
        </form>
      </div>
      </div>
    );
    }
}
 
export  {ResetPassword};