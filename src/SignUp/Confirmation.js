import React, { Component } from "react";
import "../SignUp/SignUp.css";
class Confirmation extends Component {
  state = {
    email: this.props.email,
    message: this.props.message,
    resend: false
  };

  resendCall = e => {
    e.preventDefault();
    this.setState({
      resend: true
    });
  };
  render() {
    const { message } = this.state;
    if (!this.state.resend)
      return (
        <div className="forget">
          <div className="confirm">
              <div>{message ? <h1 id="txt">{message}</h1> : null}</div>
            <form onSubmit={this.resendCall}>

              <div>
                <input
                  type="submit"
                  id="resendConfirmation"
                  onClick={this.resendCall}
                  value="Resend Email"
                />
              </div>
            </form>
          </div>
        </div>
      );
    else {
      return <ResendConfirmation email={this.state.email} />;
    }
  }
}

export default Confirmation;

class VerifyConfirmation extends Component {
  state = {};
  componentDidMount() {
    fetch("http://0.0.0.0:8080/api/account/confirmation")
      .then(res => res.json())
      .then(json => {
        //console.log("json", json);
        if (json.success) {
          this.setState({
            message: json.message
          });
        } else {
          this.setState({
            message: json.message
          });
        }
      });
  }
  render() {
    const { message } = this.state;
    console.log("inside if confirmation Email:", this.state.email);
    if (!this.state.resend) {
      return (
        <div className="forget">
          <div className="confirm">
            <div>{message ? <h1 id="txt">{message}</h1> : null}</div>
          </div>
        </div>
      );
    }
  }
}
export { VerifyConfirmation };

class ResendConfirmation extends Component {
  state = {
    email: this.props.email,
    message:"sending..."
  };
  componentWillMount() {
    console.log("flag1 Email: ", this.state.email);

    const { email } = this.state;
    fetch("http://0.0.0.0:8080/api/account/resend", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email
      })
    })
      .then(res => res.json())
      .then(json => {
        //console.log("json", json);
        if (json.success) {
          this.setState({
            message: json.message
          });
        } else {
          this.setState({
            message: json.message
          });
        }
      });
    console.log("flag2");
  }
  render() {
    console.log("flag3");

    const { message } = this.state;
    return (
      <div className="forget">
        <div className="confirm">
          <div>{message ? <h1 id="txt">{message}</h1> : null}</div>
        </div>
      </div>
    );
  }
}
