import React, { Component } from "react";
import "../SignUp/SignUp.css";
import "whatwg-fetch";
import { withRouter } from "react-router-dom";
import Confirmation from "./Confirmation";

class SignUp extends Component {
  state = {
    signUpFirstName: "",
    signUpLastName: "",
    signUpUserName: "",
    signUpEmail: "",
    signUpPassword: "",
    signUpError: "",
    confirmation:false
  };

  submit = e => {
    //
    e.preventDefault();

    const {
      signUpFirstName,
      signUpLastName,
      signUpUserName,
      signUpEmail,
      signUpPassword,
    } = this.state;

    // POST request to backend

    fetch("http://0.0.0.0:8080/api/account/signup", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        host: "localhost:3000",
        firstName: signUpFirstName,
        lastName: signUpLastName,
        userName: signUpUserName,
        email: signUpEmail,
        password: signUpPassword
      })
    })
      .then(res => res.json())
      .then(json => {
        console.log("json", json);
        if (json.success) {
          this.setState({
            signUpError: json.message,
            isLoading: false,
            confirmation:true
          }
          );
         console.log(this.state.confirmation); 
        } else {
          this.setState({
            signUpError: json.message,
            isLoading: false
          }); 
        }
        if (!json.success) {
          alert(this.state.signUpError);
        }
      });
  };

  changehandler = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    const {
      signUpFirstName,
      signUpLastName,
      signUpUserName,
      signUpEmail,
      signUpPassword,
      signUpError
    } = this.state;
    if(!this.state.confirmation)
    {

    return (
      <div className="signup">
        <div className="box">
          <form onSubmit={this.submit}>
            <div>
              <label>Name:</label>
              <input
                type="text"
                name="signUpFirstName"
                value={signUpFirstName}
                onChange={this.changehandler}
              />
            </div>
            <div>
              <label>Surname:</label>
              <input
                type="text"
                name="signUpLastName"
                value={signUpLastName}
                onChange={this.changehandler}
              />
            </div>
            <div>
              <label>Email:</label>
              <input
                type="text"
                name="signUpEmail"
                value={signUpEmail}
                onChange={this.changehandler}
              />
            </div>
            <div>
              <label>Username:</label>
              <input
                type="text"
                name="signUpUserName"
                value={signUpUserName}
                onChange={this.changehandler}
              />
            </div>
            <div>
              <label>Password:</label>
              <input
                type="password"
                name="signUpPassword"
                value={signUpPassword}
                onChange={this.changehandler}
              />
            </div>
            <div>
              <input type="submit" value="Create New Account" />
            </div>
          </form>
        </div>
      </div>
    );
  }
  else if(this.state.confirmation)
    {
      return(
        < Confirmation email={this.state.signUpEmail} message={this.state.signUpError} ></ Confirmation>
      )
    }
  }
}
export default withRouter(SignUp);
